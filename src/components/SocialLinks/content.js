const linkedin = "raptortech"
const instagram = "raptor_tech"
const twitter = "RaptorTech_"
const links = [
  {
    label: "Linkedin",
    url: `https://www.linkedin.com/company/${linkedin}`,
  },
  {
    label: "Instagram",
    url: `https://www.instagram.com/${instagram}`,
  },
  {
    label: "Twitter",
    url: `https://www.twitter.com/${twitter}`,
  },
]

export default links
