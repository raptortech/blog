const links = [
  {
    label: 'Home',
    url: '/'
  },
  {
    label: 'About Us',
    url: '/about/'
  }
]

export default links
