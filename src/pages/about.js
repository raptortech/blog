import React from 'react'

import Layout from '../components/Layout/'
import SEO from '../components/seo'
import SocialLinks from '../components/SocialLinks'

import { MainContent } from '../styles/base'

const AboutPage = () => (
  <Layout>
    <SEO
      title="About Us"
      description="RaptorTech was founded out of a passion for delivering on the promise of technology in heavy industry. Our team of specialists can help you get the most out of your investment - the first time, every time."
    />
    <MainContent>
      <h1>About us</h1>
      <p>
      RaptorTech was founded out of a passion for delivering on the promise of technology in heavy industry. Our team of specialists can help you get the most out of your investment - the first time, every time.
      You can see our products at {' '}
        <a
          href="https://www.raptorgnss.com/ecommerce"
          target="_blank"
          rel="noopener noreferrer"
        >
          Ecommerce
        </a>
        , or at our social media{' '}
        <a
          href="https://www.instagram.com/raptor_tech/"
          target="_blank"
          rel="noopener noreferrer"
        >
          where we release some photos
        </a>{' '}
        of our projects
      </p>
      <p>
        RaptorTech provides technology management services to heavy industry. Our team of highly skilled technicians and engineers can help your business deploy, maintain and improve technology solutions. We specialize in <strong>IoT</strong>, <strong>telemetry</strong>, <strong>automation</strong> and <strong>machine interface technologies</strong>. We can also help you assess the right product for your business.  
      </p>

      <h2>Contact</h2>

      <p>
        You can contact us through any of our
        social media.
      </p>

      <SocialLinks hideStyle />
    </MainContent>
  </Layout>
)

export default AboutPage